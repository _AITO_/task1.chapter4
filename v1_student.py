# Создайте класс Student, конструктор которого имеет параметры name, lastname,
# department, year_of_entrance. Добавьте метод get_student_info, который
# возвращает имя, фамилию, год поступления и факультет в
# отформатированном виде: “Вася Иванов поступил в 2017 г. на факультет:
# Программирование.

class Student():
    def __init__(self, name, lastname, department, year_of_entrance):
        self.name = name
        self.lastname = lastname
        self.year_of_entrance = year_of_entrance
        self.department = department

    def course(self):
        pass


    def get_info(self):
        return "Имя {},Фамилия {},поступил в {} году,на фокультет:{}".format(self.name,self.lastname,self.year_of_entrance,self.department)

class First_student(Student):
    def course(self):
        return "2 course"

class Second_student(Student):
    def course(self):
        return "5 course"

First_student = First_student('Ваня','Сергеевич','Бакалавра','2018')
Second_student = Second_student('Игорь','Бактыбеков','Окстворт','2016')
print(First_student.get_info())
print(Second_student.get_info())